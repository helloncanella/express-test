angular.module('app',['ngRoute'])

.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
	$routeProvider

	.when('/contacts',{
		templateUrl:'partials/contacts.jade'
	})

	.when('/banana',{
		templateUrl:'partials/banana.jade'
	})

	;

	$locationProvider.html5Mode(true);
}])