var express = require('express');
var pub = __dirname + '/public';

var app = express();

app.use(express.static(pub));

app.set('views', __dirname+'/views');

app.set('view engine', 'jade');


app.use(function(err, req, res, next) {
  res.send(err.stack);
});

app.get('/partials/:name', function (req, res) {
  var name = req.params.name;
  res.render('partials/' + name);
});

app.get('/*', function(req, res){
	res.render('index',{title:"Olá"})
});



app.listen(process.env.port || 5700);